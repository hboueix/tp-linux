# TP-dns

Ce petit TP permet de jouer avec une architecture DNS 

## Présentation 

Nous construisons via le vagrantfile :
 
- Deux serveur DNS de récurssion accessible que depuis le réseaux interne (192.168.33.0/24)
- Deux serveurs autoritatifs avec une interface web d’administration accessible sur le reseaux interne et le service dns exposer sur le réseaux publique (192.168.56.0/24)
- Le domaine lab.local est povisionné sur les serveur dns authoritatifs et les serveurs récursifs `forward` les requêtes pour ce domaine vers les serveurs autoritatifs.
- Un reverse proxy accessible depuis le réseaux publique (192.168.56.0/24)
- deux serveurs web wiki et back accessible que depuis le réseaux privé (192.168.33.0/24)

Une interface web est disponible sur les deux serveurs autoritatif via les urls :  http://192.168.33.31/poweradmin/ et http://192.168.33.32/poweradmin/ 
Login : admin ; mot de passe xxxxxx

> attention après avoir récupèrer le dépot, une seule commande à passer "vagrant up" mais les traitements peuvent être long, ne les interrompez pas.

## Questions 

- Connectez vous au host "proxy" avec vagrant et vérifier Comment est configuré le resolver dns du système ?
Dans le fichier `/etc/resolv.conf` de l'host "proxy" les serveurs dns configurés sont les hosts "recursor-1" et "recursor-2".
- Retrouvez l'adresse ip du host wiki.lab.local avec la commande dig.
```
vagrant@proxy:~$ dig wiki.lab.local

; <<>> DiG 9.10.3-P4-Debian <<>> wiki.lab.local
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 28209
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;wiki.lab.local.			IN	A

;; ANSWER SECTION:
wiki.lab.local.		3600	IN	A	192.168.56.11  <--- On la retrouve juste ici

;; Query time: 10 msec
;; SERVER: 192.168.33.21#53(192.168.33.21) <-- serveur dns de récursion utilisé
;; WHEN: Sun Oct 20 20:35:09 GMT 2019
;; MSG SIZE  rcvd: 59

```
- Connectez vous au host auth-1, quels sont les services réseaux qui sont en fonctionnement actuellement quels sont leur socket d'écoute ?
```
[vagrant@auth-1 ~]$ sudo ss state listening -laputn4
Netid Recv-Q Send-Q          Local Address:Port                         Peer Address:Port              
tcp   0      50                  127.0.0.1:3306                                    *:*                   users:(("mysqld",pid=2803,fd=14))
tcp   0      128                         *:111                                     *:*                   users:(("rpcbind",pid=2128,fd=4),("systemd",pid=1,fd=36))
tcp   0      128             192.168.33.31:80                                      *:*                   users:(("httpd",pid=4871,fd=3),("httpd",pid=2842,fd=3),("httpd",pid=2841,fd=3),("httpd",pid=2840,fd=3),("httpd",pid=2839,fd=3),("httpd",pid=2838,fd=3),("httpd",pid=2527,fd=3))
tcp   0      128                         *:53                                      *:*                   users:(("pdns_server",pid=3098,fd=7))
tcp   0      128                         *:22                                      *:*                   users:(("sshd",pid=2530,fd=3))
tcp   0      100                 127.0.0.1:25                                      *:*                   users:(("master",pid=2824,fd=13))
```
Les services réseaux en fonctionnement sont donc :
	- mysqld, pour les bases de données
	- rpcbind, NFS pour "monter" des systèmes de fichiers distants comme s'ils étaient en local
	- httpd, serveur web
	- pdns_server, serveur dns authoritatif
	- sshd (evidemment)
	- master, postfix master process
	
- Connectez vous au host recursor-1,  quels sont les services réseaux qui sont en fonctionnement actuellement quels sont leur socket d'écoute ?
```
[vagrant@recursor-1 ~]$ sudo ss -laputen4 state listening
Netid Recv-Q Send-Q                                                                Local Address:Port                                                                               Peer Address:Port              
tcp   0      128                                                                               *:111                                                                                           *:*                   users:(("rpcbind",pid=1297,fd=4),("systemd",pid=1,fd=64)) ino:15489 sk:ffff8e0a13f6c000 <->
tcp   0      128                                                                   192.168.33.21:53                                                                                            *:*                   users:(("pdns_recursor",pid=5414,fd=7)) ino:32799 sk:ffff8e0a167c26c0 <->
tcp   0      128                                                                       127.0.0.1:53                                                                                            *:*                   users:(("pdns_recursor",pid=5414,fd=6)) ino:32798 sk:ffff8e0a167c0000 <->
tcp   0      128                                                                               *:22                                                                                            *:*                   users:(("sshd",pid=2370,fd=3)) ino:21075 sk:ffff8e0a13f6c7c0 <->
tcp   0      100                                                                       127.0.0.1:25                                                                                            *:*                   users:(("master",pid=2583,fd=13)) ino:21755 sk:ffff8e0a13f6cf80 <->
```
Les services réseaux en fonctionnement sont donc :
	- rpcbind, NFS
	- pdns_recursor, serveur dns de récursion
	- 2 fois ?
	- encore une fois bien sûr sshd 
	- master, Postfix master process

- Où sont configuré chacuns de ces composants ?
	- MySQL ça va être les fichiers `/etc/my.cnf` et `/etc/my.cnf.d/*`.  
	- rpcbind : pas vraiment de config à part peut-être dans `/etc/sysconfig/rpcbind`
	- httpd : dans `/etc/httpd/conf/httpd.conf`
	- pdns_server : dans `/etc/pdns/pdns.conf`
	- pdns_recursor : dans `/etc/pdns-recursor/recursor.conf`
	- master : dans `/usr/libexec/postfix/master.cf`
- Qu'est ce qui est configuré sur les serveurs recursifs pour le domaine lab.local ? (Important pour les Actions qui suivent)
On peut remarquer cette ligne dans le fichier `/etc/pdns-recursor/recursor.conf` :
```
forward-zones=lab.local=192.168.56.31;192.168.56.32
```
Les requêtes pour le lab.local seront *forward* vers les IPs configurées, à savoir les hosts "auth-1" et "auth-2".
- Comment mettre en évidence le fait que le récurseurs ne répondent que sur l’interface du réseaux back (192.168.33.0/24) ?
La ligne juste au dessus nous indique que le serveur n'autorise que les connexions du réseau local et du réseau back :
```
allow-from=127.0.0.0/8, 192.168.33.0/24
```
- Comment est sécurisé l’accès à mysql ?
MySQL est lancé par les serveurs authoritatifs et dans leur fichier de config `/etc/pdns/pdns.conf` on crée un utilisateur avec un password :
```
launch=gmysql
gmysql-host=127.0.0.1
gmysql-user=pdns
gmysql-dbname=pdns
gmysql-password=zzzzzz
master=yes
slave=yes
```

## Actions

- Ajoutez un nouveau domaine "preprod.local" sur les serveurs autoritatifs interrogeable au travers des serveurs recursifs, décrivez vos actions.
- Mettez en place une config reverse-proxy pour les interfaces poweradmin. Vous préciserez vos actions et dans la configuration reverse proxy le socket d'écoute, le serveur name et la route web configuré.
- Mettez en place une sauvegarde des données dns.
- Mettez en place un second site wiki hébergé de la même manière (sur wiki et back) mais pour le domaine preprod.local




